package com.ntently.fidelis.utils;

import com.google.inject.servlet.RequestScoped;

import java.util.*;

/**
 * Copyright 2015/01 www.ntently.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author <a href="richard@ntently.com">Richard M.</a>
 *
 */

@RequestScoped
public class Messages extends ResourceBundle {

    private ResourceBundle bundle;
    private Map<String, String> messages;

    public Messages(Locale locale) {
        bundle = PropertyResourceBundle.getBundle("bundles/messages", locale);
        messages = new HashMap<>();

        Enumeration<String> keys = bundle.getKeys();
        while(keys.hasMoreElements()) {
            String key = keys.nextElement();
            messages.put(key, bundle.getString(key));
        }
    }

    @Override
    protected Object handleGetObject(String key) {
        return null;
    }

    public Map<String, String> getMessages() {
        return messages;
    }

    @Override
    public Enumeration<String> getKeys() {
        return Collections.enumeration(bundle.keySet());
    }
}
