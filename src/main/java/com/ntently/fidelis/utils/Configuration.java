package com.ntently.fidelis.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Copyright 2015/02 www.ntently.com
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author <a href="richard@ntently.com">Richard M.</a>
 */
public class Configuration {
    private Properties properties = new Properties();

    public Configuration() {
        InputStream input = null;
        try {
            input = this.getClass().getResourceAsStream("bundles/email/configuration.properties");

            if(input != null) {
                properties.load(input);
            } else {
                throw new FileNotFoundException("CANNOT FIND THE PROPERTIES FILE");
            }

            System.out.print(properties.getProperty("key"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(input != null) {
                try {
                    input.close();
                } catch(IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String getConfiguration(String config) {
        return properties.getProperty(config);
    }
}
