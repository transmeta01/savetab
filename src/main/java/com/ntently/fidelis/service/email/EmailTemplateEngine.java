package com.ntently.fidelis.service.email;

import com.google.inject.ImplementedBy;

/**
 * Created by richard on 15-02-23.
 */
@ImplementedBy(MustacheEmailTemplateEngine.class)
public interface EmailTemplateEngine {
    String buildEmailBody();
}

