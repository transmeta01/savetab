package com.ntently.fidelis.service.email;

import org.trimou.engine.MustacheEngine;
import org.trimou.engine.MustacheEngineBuilder;

import java.util.logging.Logger;

/**
 * Created by richard on 15-02-23.
 */
public class MustacheEmailTemplateEngine implements EmailTemplateEngine {
    private Logger logger = Logger.getLogger(this.getClass().getName());

    private final MustacheEngine engine;

    public MustacheEmailTemplateEngine() {
        this.engine = MustacheEngineBuilder.newBuilder().build();
    }

    // TODO implement me
    @Override
    public String buildEmailBody() {
        return null;
    }
}
