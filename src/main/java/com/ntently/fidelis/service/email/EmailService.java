package com.ntently.fidelis.service.email;

import com.google.inject.ImplementedBy;
import com.ntently.fidelis.model.User;

import java.util.List;
import java.util.Map;

/**
 * Copyright 2015/01 www.ntently.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author <a href="richard@ntently.com">Richard M.</a>
 *
 */

@ImplementedBy(FidelisEmailService.class)
public interface EmailService {
    public void send(User user, Map<String, String> url, boolean isInviter);
}
