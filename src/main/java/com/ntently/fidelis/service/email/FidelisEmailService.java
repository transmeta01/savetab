package com.ntently.fidelis.service.email;

import com.google.gson.JsonElement;
import com.google.inject.name.Named;
import com.ntently.fidelis.model.User;
import org.trimou.Mustache;
import org.trimou.engine.MustacheEngine;
import org.trimou.engine.MustacheEngineBuilder;

import javax.inject.Inject;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.util.*;

/**
 * Copyright 2015/01 www.ntently.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author <a href="richard@ntently.com">Richard M.</a>
 *
 */
public class FidelisEmailService implements EmailService {

    @Inject private EmailTemplateEngine templateEngine;

    private String fromEmail;
    private String conciergeEmail;

    @Inject
    public FidelisEmailService(@Named("promo_email")String fromEmail, @Named("concierge_email") String conciergeEmail) {
        this.fromEmail = fromEmail;
        this.conciergeEmail = conciergeEmail;
    }

    // send user name, email and links to coupon
    public void send(User user, Map<String, String> url, boolean isInviter) {
        Properties properties = new Properties();
        Session session = Session.getDefaultInstance(properties, null);

        try{
            Message msg = new MimeMessage(session);
            msg.setSubject("New user registration");
            msg.setFrom(new InternetAddress(fromEmail, "registration user"));
            msg.setRecipient(Message.RecipientType.TO,
                    new InternetAddress(conciergeEmail, "Mr Concierge"));

            StringBuilder builder = new StringBuilder();
            builder.append("username : ").append(user.getUsername()).append("\n")
                    .append("email : ").append(user.getEmail()).append("\n")
                    .append("language : ").append(user.getLanguage())
                    .append("\n\n\n");

            List<String> keys = new ArrayList<>(url.keySet());
            for(String key : keys) {
                builder.append("channel : ").append(key).append(",  ")
                        .append("url : ").append(url.get(key)).append("\n");
            }

            if(isInviter) {
                builder.append("\n\nreward user for successful invitation");
            }

            builder.append("\n\n\npowered by savetab.com");

            msg.setText(builder.toString());
            Transport.send(msg);

        } catch (AddressException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch(UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }

    private String buildBody(JsonElement data) {

        //
        MustacheEngine engine = MustacheEngineBuilder
                                        .newBuilder()
                                        .build();

        // get localized template
        Mustache mustache = engine.getMustache("some_where.mustache");

        return mustache.render(data);
    }
}
