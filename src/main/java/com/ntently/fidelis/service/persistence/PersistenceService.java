package com.ntently.fidelis.service.persistence;

import com.googlecode.objectify.Key;

/**
 * Created by richard on 15-02-24.
 */
public interface PersistenceService<T> {
    public T get(String userId);

    public T get(Long userId) ;

    public Key<T> getKey(Long id) ;

    public Key<T> getKey(String id);

    public void save(T entity) ;
}
