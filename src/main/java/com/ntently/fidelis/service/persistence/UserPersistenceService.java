package com.ntently.fidelis.service.persistence;

import com.googlecode.objectify.Key;
import com.ntently.fidelis.model.User;

import static com.ntently.fidelis.service.persistence.OfyService.ofy;

/**
 * Created by richard on 15-02-24.
 */
public class UserPersistenceService implements PersistenceService<User> {

    public User get(String userId) {
        return get(Long.valueOf(userId));
    }

    public User get(Long userId) {
        return ofy().load().type(User.class).id(userId).now();
    }

    public Key<User> getKey(Long userId) {
        return Key.create(User.class, userId);
    }

    public Key<User> getKey(String userId) {
        return getKey(Long.valueOf(userId));
    }

    public void save(User user) {
        ofy().save().entity(user).now();
    }
}
