package com.ntently.fidelis.service;

import com.google.inject.Inject;
import com.ntently.fidelis.model.Coupon;
import com.ntently.fidelis.model.LongURL;
import com.ntently.fidelis.model.ShortURL;
import com.ntently.fidelis.model.User;
import com.ntently.fidelis.service.email.EmailService;
import com.ntently.fidelis.service.persistence.UserPersistenceService;
import com.ntently.fidelis.service.url.URLService;
import com.ntently.fidelis.validation.EmailValidator;

import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import static com.ntently.fidelis.service.persistence.OfyService.ofy;

/**
 * Copyright 2015/02 www.ntently.com
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author <a href="richard@ntently.com">Richard M.</a>
 */
public class FidelisCoreService implements CoreService {
    private static final Logger logger = Logger.getLogger(FidelisCoreService.class.getName());

    private final String baseURL;

    @Inject private EmailService emailService;
    @Inject private EmailValidator emailValidation;
    @Inject private URLService urlService;
    @Inject private UserPersistenceService userPersistenceService;

    @Inject
    public FidelisCoreService(@Named("base_url") String base) {
        this.baseURL = base;
    }

    @Override
    public void registerUser(String email, HttpServletRequest request) {
        // extract default user name from email
        String username = email == null ? null : email.substring(0, email.indexOf("@"));

        // if these are present, record for analytics
        String requestCoupon = request.getParameter("coupon");
        String requestChannel = request.getParameter("ch");
        String inviterId = request.getParameter("i");

        String language  = request.getLocale().getLanguage();

        // TODO retrieve localized email template
        Map<String, String> localized = (HashMap<String, String>)request.getAttribute("messages");

        // ignore registration
        int count = ofy().load().type(User.class).filter("email", email).list().size();
        if((count < 1) && !email.contains("test")) {
            User newUser = new User(username, email, language);
            ofy().save().entity(newUser).now();

            if(inviterId != null && "".equals(inviterId)) {
                rewardInviter(inviterId, newUser);
            }

            // generate a coupon for the user
            Coupon coupon = generateCoupon(newUser);

            String facebookSharingURL = buildURL(newUser.getId().toString(), coupon.getId().toString(), "facebook");
            String inboxSharingURL = buildURL(newUser.getId().toString(), coupon.getId().toString(), "email");

            // generate sharing URLs
            ShortURL couponFacebookURL = urlService.requestURLShortening(new LongURL(facebookSharingURL));
            ShortURL couponEmailURL = urlService.requestURLShortening(new LongURL(inboxSharingURL));

            // for now we only handle 2 channels
            coupon.addURL("facebook", couponFacebookURL.getId());
            coupon.addURL("email", couponEmailURL.getId());

            notifyUser(newUser, coupon.getURLMap(), Boolean.FALSE);
        }
    }

    /**
     * Generate a record of coupon and associate the coupon to the user
     */
    public Coupon generateCoupon(User user) {
        // generate a coupon for the user
        Coupon coupon = new Coupon();
        coupon.setOwner(userPersistenceService.getKey(user.getId()));

        // this should be automated
        coupon.setDescription("Coupon for Loup Garou promo");
        ofy().save().entity(coupon).now();

        return coupon;
    }

    private String buildURL(String userId, String couponId, String channel) {
        StringBuilder builder = new StringBuilder(baseURL).append("?");
        builder.append("i=").append(userId).append("&")
                .append("coupon=").append(couponId).append("&")
                .append("ch=").append(channel);

        return builder.toString();
    }

    /**
     * reward and record invitation
     *
     * @param inviterId
     */
    public void rewardInviter(String inviterId, User newUser) {
        // retrieve inviter user
        User inviter = ofy().load().type(User.class).id(Long.valueOf(inviterId)).now();

        // save the relationship
        newUser.setInviter(userPersistenceService.getKey(inviterId));
        userPersistenceService.save(newUser);

        // has this guy invited at least 3 people
        int count = ofy().load().type(User.class)
                         .filterKey("inviterId", userPersistenceService.getKey(inviterId))
                         .list().size();

        if(count >= 3) {
            // generator congratulatory coupon
            Coupon coupon = generateCoupon(inviter);
            String url = buildURL(inviter.getId().toString(), coupon.getId().toString(), "savetab");

            ShortURL shortURL = urlService.requestURLShortening(new LongURL(url));
            coupon.addURL("savetab", shortURL.getId());
            coupon.setOwner(userPersistenceService.getKey(inviter.getId()));
            ofy().save().entity(coupon).now();

            notifyUser(inviter, coupon.getURLMap(), Boolean.TRUE);
        }
    }

    public void notifyUser(User user, Map<String, String> urlMap, boolean isInviter) {
//        emailService.send(user, urlMap, isInviter);
    }
}
