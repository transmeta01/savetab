package com.ntently.fidelis.service.url;

import com.ntently.fidelis.model.LongURL;
import com.ntently.fidelis.model.ShortURL;
import retrofit.http.*;

/**
 * Copyright 2015/02 www.ntently.com
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author <a href="richard@ntently.com">Richard M.</a>
 */
public interface GoogleURLShortenerService {
    @Headers("Content-Type : application/json")
    @POST("/urlshortener/v1/url")
    ShortURL getShortURL(@Query("key") String apiKey, @Body LongURL url);
}
