package com.ntently.fidelis.service.url;

import javax.inject.Inject;
import com.ntently.fidelis.model.LongURL;
import com.ntently.fidelis.model.ShortURL;
import retrofit.RestAdapter;

import javax.inject.Named;
import javax.inject.Singleton;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Copyright 2015/02 www.ntently.com
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author <a href="richard@ntently.com">Richard M.</a>
 */

@Singleton
public class CoreURLService implements URLService {
    private Logger logger = Logger.getLogger(CoreURLService.class.getName());

    private String apiKey;
    private String urlServiceEndpoint;

    public CoreURLService() {
    }

    @Inject
    public CoreURLService(@Named("key")String apiKey, @Named("apiURL")String urlServiceEndpoint) {
        this.apiKey = apiKey;
        this.urlServiceEndpoint = urlServiceEndpoint;
    }

    @Override
    public ShortURL requestURLShortening(LongURL longURL) {
        RestAdapter adapter = new RestAdapter.Builder()
                                            .setEndpoint(urlServiceEndpoint)
                                            .build();

        GoogleURLShortenerService shortUrlService = adapter.create(GoogleURLShortenerService.class);

        return shortUrlService.getShortURL(apiKey, longURL);
    }
}
