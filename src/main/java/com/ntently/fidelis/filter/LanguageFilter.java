package com.ntently.fidelis.filter;

import com.ntently.fidelis.utils.Messages;

import javax.inject.Singleton;
import javax.servlet.*;
import java.io.IOException;
import java.util.Map;

/**
 * Copyright 2015/01 www.ntently.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author <a href="richard@ntently.com">Richard M.</a>
 *
 */
@Singleton
public class LanguageFilter implements Filter {
    private FilterConfig filterConfig = null;
    private String MESSAGES_KEY = "messages";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        if(servletRequest.getAttribute(MESSAGES_KEY) == null) {
            Messages messages = new Messages(servletRequest.getLocale());
            servletRequest.setAttribute(MESSAGES_KEY, messages.getMessages());
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
     // do nothing
    }
}
