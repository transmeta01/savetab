package com.ntently.fidelis.integration;

import com.google.inject.name.Names;
import com.google.inject.servlet.ServletModule;
import com.googlecode.objectify.ObjectifyFilter;
import com.ntently.fidelis.filter.CORSFilter;
import com.ntently.fidelis.filter.LanguageFilter;
import com.ntently.fidelis.service.email.EmailTemplateEngine;
import com.ntently.fidelis.service.persistence.OfyService;
import com.ntently.fidelis.service.persistence.UserPersistenceService;
import com.ntently.fidelis.servlet.BaseServlet;
import com.ntently.fidelis.servlet.RegisterServlet;

import javax.inject.Singleton;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Copyright 2015/01 www.ntently.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author <a href="richard@ntently.com">Richard M.</a>
 *
 * Module configuration for the web app
 *
 */
public final class FidelisServletModule extends ServletModule {
    private Logger logger = Logger.getLogger(FidelisServletModule.class.getName());

    @Override
     public void configureServlets() {
        filter("/*").through(CORSFilter.class);
        filter("/*").through(LanguageFilter.class);
        filter("/*").through(ObjectifyFilter.class);
        serve("/landing").with(BaseServlet.class);
        serve("/register").with(RegisterServlet.class);

        bind(ObjectifyFilter.class).in(Singleton.class);
        requestStaticInjection(OfyService.class);
        binder().requestInjection(UserPersistenceService.class);
        bind(EmailTemplateEngine.class);

        try {
            Names.bindProperties(binder(), loadProperties());
        } catch (IOException e) {
            logger.log(Level.SEVERE, e.getMessage());
        }
    }

    private Properties loadProperties() throws IOException {
        Properties properties = new Properties();
        InputStream input = this.getClass().getClassLoader().getResourceAsStream("bundles/config/configuration.properties");
        properties.load(input);

        return properties;
    }
}
