package com.ntently.fidelis.servlet;

import com.google.inject.Inject;
import com.ntently.fidelis.service.FidelisCoreService;
import com.ntently.fidelis.validation.EmailValidator;

import javax.inject.Singleton;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Copyright 2015/01 www.ntently.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author <a href="richard@ntently.com">Richard M.</a>
 *
 */

@Singleton
public class RegisterServlet extends HttpServlet {

    @Inject private FidelisCoreService service;
    @Inject private EmailValidator emailValidator;

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    private void process(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String email = request.getParameter("email");

        // validate email
        if((email != null && !"".equals(email)) && emailValidator.validate(email)) {
           service.registerUser(email, request);

            request.getRequestDispatcher("WEB-INF/jsp/thank_register.jsp").forward(request, response);
        }
    }
}
