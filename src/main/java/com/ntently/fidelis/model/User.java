package com.ntently.fidelis.model;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Load;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Copyright 2015/01 www.ntently.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author <a href="richard@ntently.com">Richard M.</a>
 *
 */

@Entity
public class User {
    @Id private Long id;
    private String username;
    @Index  private String email;
    @Index private  String language;

    @Index private Date created;

    @Load private List<Key<Coupon>> couponList = new ArrayList<>();

    @Index private Key<User> inviter;

    public User() {
    }

    public User(String username, String email, String language) {
        this.username = username;
        this.email = email;
        this.language = language;
        this.created = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public User setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public User setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getLanguage() {
        return language;
    }

    public User setLanguage(String language) {
        this.language = language;
        return this;
    }

    public void addCoupon(Coupon coupon) {
        couponList.add(Key.create(Coupon.class, coupon.getId()));
    }

    public List<Key<Coupon>> getCouponList() {
        return couponList;
    }

    public void setCouponList(List<Key<Coupon>> couponList) {
        this.couponList = couponList;
    }

    public Key<User> getInviter() {
        return inviter;
    }

    public void setInviter(Key<User> inviter) {
        this.inviter = inviter;
    }
}
