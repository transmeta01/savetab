package com.ntently.fidelis.model;

import com.google.appengine.api.datastore.Blob;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Load;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Copyright 2015/02 www.ntently.com
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author <a href="richard@ntently.com">Richard M.</a>
 *
 * This domain model object allows to measure the walk-in and redeem of user gesture. This
 * data is the basis for the current version of the analytics engine
 */

@Entity
public class Coupon {
    @Id Long id;
    @Index private boolean redeemed;
    @Index private Date dateRedeemed;

    private Blob image;
    private String description;

    @Index private Date created;
    @Index private Key<User> owner;

    @Load private Map<String, String> urlMap = new HashMap<>();

    public Coupon() {
        this.created = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isRedeemed() {
        return redeemed;
    }

    public void setRedeemed(boolean redeemed) {
        this.redeemed = redeemed;
    }

    public Date getDateRedeemed() {
        return dateRedeemed;
    }

    public void setDateRedeemed(Date dateRedeemed) {
        this.dateRedeemed = dateRedeemed;
    }

    public Blob getImage() {
        return image;
    }

    public void setImage(Blob image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Key<User> getOwner() {
        return owner;
    }

    public void setOwner(Key<User> owner) {
        this.owner = owner;
    }

    public void addURL(String channel, String url) {
        urlMap.put(channel, url);
    }

    public Map<String, String> getURLMap() { return urlMap; }
}
