package com.ntently.fidelis.model;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

/**
 * Copyright 2015/02 www.ntently.com
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author <a href="richard@ntently.com">Richard M.</a>
 */
@Entity
public enum Channel {
    MERCHANT_FACEBOOK_PAGE("MERCHANT_FACEBOOK_PAGE"),
    MERCHANT_INSTORE("MERCHANT_INSTORE"),
    FRIEND_FACEBOOK_INVITE("FRIEND_FACEBOOK_INVITE"),
    FRIEND_EMAIL_INVITE("FRIEND_EMAIL_INVITE");

    private String description;

    @Id Long id;

    Channel(String description) {
        this.description = description;
    }
}
