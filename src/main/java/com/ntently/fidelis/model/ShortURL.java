package com.ntently.fidelis.model;

import com.googlecode.objectify.annotation.Index;

/**
 * Copyright 2015/02 www.ntently.com
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author <a href="richard@ntently.com">Richard M.</a>
 *
 * This is the class representing the response from Google URL Shortener API
 */

public class ShortURL {
    private String kind;

    private String id;

    private String longUrl;

    public ShortURL() {
    }

    public ShortURL(String kind, String id, String longUrl) {
        this.kind = kind;
        this.id = id;
        this.longUrl = longUrl;
    }

    public String getKind() {
        return kind;
    }

    public String getId() {
        return id;
    }

    public String getLongUrl() {
        return longUrl;
    }
}
