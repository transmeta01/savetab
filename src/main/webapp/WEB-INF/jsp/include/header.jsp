<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="HandheldFriendly" content="true" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">

    <meta name="description" content="savetab">
    
    <!-- Chrome & Firefox OS -->
    <meta name="theme-color" content="#4285f4">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#4285f4">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#4285f4">

    <title></title>

    <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.5.0/pure-min.css">
	<link rel="icon" type="image/png" href="images/favicon.ico" />

    <!--[if lte IE 8]>
    <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.5.0/grids-responsive-old-ie-min.css">
    <![endif]-->

    <!--[if gt IE 8]><!-->

    <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.5.0/grids-responsive-min.css">

    <!--<![endif]-->

    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>

    <link href="${pageContext.servletContext.contextPath}/css/layouts/marketing-old-ie.css" rel="stylesheet"/>
    <link href="${pageContext.servletContext.contextPath}/css/layouts/marketing.css" rel="stylesheet"/>
    <link href="${pageContext.servletContext.contextPath}/css/style.css" type="text/css" rel="stylesheet" media="screen"/>
    <link href="${pageContext.servletContext.contextPath}/js/validation.js" type="text/javascript"/>

</head>
<body>
    <div id="header">
        <div class="logo"><a href="/"><img src="images/savetab-circle.png" alt="savetab logo" height="160px" width="200px"/></a></div>
        <div id="beta">invite beta version</div>
    </div>