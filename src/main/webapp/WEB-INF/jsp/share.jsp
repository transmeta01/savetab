<%@ include file="include/header.jsp" %>

   <div class="content">
            <h1>${messages["share.benefit"]}</h1>
            <div class="pure-g">
                        <div class="l-box-lrg pure-u-1 pure-u-md-2-5">
                            <form class="pure-form pure-form-stacked" action="/process_sharing" method="POST" name="share">
                                <fieldset>
                                    <input name="inviter" type="hidden" value='<%= request.getParameter("inviter") %>' />
                                    <input name="lang" type="hidden" value='<%= request.getParameter("lang") %>' />
                                    <label for="email1">${messages["share.label.friend1"]}</label>
                                    <input id="email" type="email" name="email1" placeholder='${messages["share.label.friend1"]}' required autocomplete="email">

                                    <label for="email2">${messages["share.label.friend2"]}</label>
                                     <input id="email" type="email" name="email2" placeholder='${messages["share.label.friend2"]}' required autocomplete="email">

                                      <label for="email3">${messages["share.label.friend3"]}</label>
                                      <input id="email" type="email" name="email3" placeholder='${messages["share.label.friend2"]}' required autocomplete="email">

                                      <button type="submit" class="pure-button">${messages["register.submit.button.label"]}</button>
                                </fieldset>
                            </form>
                        </div>
            </div>
   </div>

<%@ include file="include/footer.jsp" %>