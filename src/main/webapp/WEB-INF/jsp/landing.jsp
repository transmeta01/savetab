<%@ include file="include/header.jsp" %>

   <div class="content">
        <div id="action">
            <p>${messages["register.benefit1"]}</p>
            <p>${messages["register.benefit2"]}</p>
        </div>

        <c:if test="${not empty message.error.email}">
            <div id="error">${message.error.email}</div>
        </c:if>

            <!-- add logic to show promo code box -->

            <div class="pure-g">
                        <div id ="form" class="l-box-lrg pure-u-1 pure-u-md-2-5">
                            <form class="pure-form pure-form-stacked" action="/register" method="POST" name="register">
                                    <input name="channel" type="hidden" value='<%= request.getParameter("ch") %>' />
                                    <input name="coupon" type="hidden" value='<%= request.getParameter("coupon") %>' />
                                    <input name="i" type="hidden" value='<%= request.getParameter("i") %>' />
                                    <label for="email">${messages["register.email.label"]}</label>
                                    <input id="email" type="email" name="email" placeholder='${messages["register.email.placeholder.label"]}' required autocomplete="email">
                                    <div id="email-error-msg"></div>
                                    <button type="submit" class="pure-button">${messages["register.submit.button.label"]}</button>
                            </form>
                        </div>
            </div>
   </div>

<%@ include file="include/footer.jsp" %>