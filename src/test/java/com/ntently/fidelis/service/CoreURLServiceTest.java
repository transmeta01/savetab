package com.ntently.fidelis.service;

import com.ntently.fidelis.model.LongURL;
import com.ntently.fidelis.model.ShortURL;
import com.ntently.fidelis.service.url.CoreURLService;
import com.ntently.fidelis.service.url.URLService;
import junit.framework.TestCase;
import org.jukito.JukitoModule;
import org.jukito.JukitoRunner;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

@RunWith(JukitoRunner.class)
public class CoreURLServiceTest extends TestCase {

    @Inject
    URLService testSubject;

    @Before
    public void setUp() throws Exception {
        super.setUp();

    }

    @After
    public void tearDown() throws Exception {
        assertTrue(Boolean.TRUE);
    }

    @Test
    public void shouldReturnAShortUrl() {
        final LongURL testURL = new LongURL("www.savetab.com/landing?merchant=Loup+Garou?channel=facebook");
        final ShortURL shortURL = testSubject.requestURLShortening(testURL);

        assertTrue(Boolean.TRUE);
    }

    public static class Tester extends JukitoModule {

        @Override
        protected void configureTest() {
            bindSpy(CoreURLService.class);
        }
    }
}